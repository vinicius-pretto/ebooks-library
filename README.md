# Ebooks Library

## Setup

Create an env file and add the folling variables:

REACT_APP_API_HOST='http://localhost:8080'

## Running Application

### Install Dependencies

```
$ npm install
```

### Start

```
$ npm start
```

### Storybook

```
$ npm run storybook
```

## [Prototipos](https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FR2ZpMG10Pg5Lo6kgQKmwRA2P%2FEbooks-Library%3Fnode-id%3D0%253A1%26scaling%3Dmin-zoom)

Os prototipos foram feitos com a ferramenta [Figma](https://www.figma.com/)
