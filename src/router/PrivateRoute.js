import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Routes from '../router/Routes';
import AuthService from '../services/AuthService';

const authService = new AuthService();

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        authService.isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: Routes.LOGIN,
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
