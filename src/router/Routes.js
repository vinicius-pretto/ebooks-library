const Routes = {
  HOME: "/",
  BOOK_DETAILS: "/books/:id/details",
  MY_BOOKS: "/my-books",
  BOOKS_REGISTER: "/books/register",
  BOOKS: "/books",
  BOOKS_EDIT: '/books/:id/edit',
  LOGIN: '/login',
  USERS_REGISTER: '/users/register'
};

export default Routes;
