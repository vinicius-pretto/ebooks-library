import axios from "axios";
import _isEmpty from 'lodash/isEmpty';

import config from "../config";

const ACCESS_TOKEN = 'x-access-token';
const USER = 'user';

class AuthService {
  removeAccessToken() {
    window.sessionStorage.removeItem(ACCESS_TOKEN);
  }

  getAccessToken() {
    return window.sessionStorage.getItem(ACCESS_TOKEN);
  }

  getCurrentUser() {
    try {
      const user = window.sessionStorage.getItem(USER);
      const userJson = JSON.parse(user);
      return userJson || {};
    } catch (error) {
      console.error(error);
      return {};
    }
  }

  isAuthenticated() {
    const accessToken = this.getAccessToken();
    const currentUser = this.getCurrentUser();
    return !_isEmpty(accessToken) && !_isEmpty(currentUser);
  }

  async auth(credentials) {
    const response = await axios.post(`${config.apiHost}/v1/auth`, credentials);
    const { token, user } = response.data;
    window.sessionStorage.setItem(ACCESS_TOKEN, token);
    window.sessionStorage.setItem(USER, JSON.stringify(user));
    return response.data; 
  }

  destroySession() {
    window.sessionStorage.removeItem(ACCESS_TOKEN);
    window.sessionStorage.removeItem(USER);
  }
}

export default AuthService;
