import axios from "axios";
import moment from 'moment';
import config from "../config";
import AuthService from './AuthService';

const US_DATE_PATTERN = 'YYYY/MM/DD';
const BRAZIL_DATE_PATTERN = 'DD/MM/YYYY';

class BooksService {
  constructor() {
    this.authService = new AuthService();
    const accessToken = this.authService.getAccessToken();
    this.httpClient = axios.create({
      baseURL: config.apiHost,
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    });
  }

  async uploadFile(file) {
    const formData = new FormData();
    formData.append("file", file);
    const response = await this.httpClient.post('/v1/upload', formData);
    return response.data.file;
  }

  async createBook(book) {
    const response = await this.httpClient.post('/v1/books', book);
    return response.data;
  }

  async getBooks() {
    const response = await this.httpClient.get('/v1/books');
    return response.data.books;
  }

  async getBorrowedBooks(userId) {
    const response = await this.httpClient.get(`/v1/users/${userId}/books`);
    return response.data.books;
  }

  async getBookById(bookId) {
    const response = await this.httpClient.get(`/v1/books/${bookId}`);
    return response.data;
  }
  
  async borrowBook(bookId) {
    const response = await this.httpClient.put(`/v1/books/${bookId}/borrow`);
    const deliveryDate = response.data.deliveryDate;
    return moment(deliveryDate, US_DATE_PATTERN).format(BRAZIL_DATE_PATTERN);
  }

  updateBook(bookId, bookToUpdate) {
    return this.httpClient.put(`/v1/books/${bookId}`, bookToUpdate);
  }

  removeBook(bookId) {
    return this.httpClient.delete(`/v1/books/${bookId}`);
  }
}

export default BooksService;
