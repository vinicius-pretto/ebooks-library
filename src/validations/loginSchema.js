import * as yup from "yup";

const validationSchema = yup.object().shape({
  username: yup.string("Usuário inválido").email("Usuário inválido").required("Usuário inválido"),
  password: yup.string("Senha inválida").min(3, "Senha inválida").required("Senha inválida")
});

export default validationSchema;
