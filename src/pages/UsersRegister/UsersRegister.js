import React from 'react';
import styled from 'styled-components';

import Input from '../../components/Input';
import Button from '../../components/Button';
import Device from '../../styles/Device';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 1%;
  margin-bottom: 1%;
`
Container.displayName = 'Container';

const Form = styled.form`
  background: #FFF;
  padding: 20px;
  width: 500px;
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  box-shadow: 5px 5px 5px #ccc;

  @media ${Device.MOBILE} {
    width: 80%;
    padding: 20px;
  }
`
Form.displayName = 'Form';

const Title = styled.h1`
  text-align: center;
  margin-top: 0;
  margin-bottom: 30px;
  font-size: 25px;
`
Title.displayName = 'Title';


const UsersRegister = (props) => {
  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit
  } = props;

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Title>Inscrever-se</Title>
        <Input
          id="firstName"
          type="text"
          name="firstName"
          label="Nome"
          placeholder="Nome"
          value={values.firstName}
          error={errors.firstName}
          touched={touched.firstName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="lastName"
          type="text"
          name="lastName"
          label="Sobrenome"
          placeholder="Sobrenome"
          value={values.lastName}
          error={errors.lastName}
          touched={touched.lastName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="email"
          type="text"
          name="email"
          label="Email"
          placeholder="Email"
          value={values.email}
          error={errors.email}
          touched={touched.email}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="birthDate"
          type="text"
          name="birthDate"
          label="Data de nascimento"
          placeholder="Data de nascimento"
          value={values.birthDate}
          error={errors.birthDate}
          touched={touched.birthDate}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="telephone"
          type="text"
          name="telephone"
          label="Telefone"
          placeholder="Telefone"
          value={values.telephone}
          error={errors.telephone}
          touched={touched.telephone}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="password"
          type="text"
          name="password"
          label="Senha"
          placeholder="Senha"
          value={values.password}
          error={errors.password}
          touched={touched.password}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Button type="submit" primary>Cadastrar</Button>
      </Form>
    </Container>  
  )
}

export default UsersRegister;
