import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import Input from "../../components/Input";
import Button from "../../components/Button";
import Routes from "../../router/Routes";
import ErrorMessage from "../../components/ErrorMessage";

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
`;
Container.displayName = "Container";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  background: #fff;
  padding: 25px;
  width: 250px;
  border-radius: 4px;
  box-shadow: 5px 5px 5px #ccc;
`;
Form.displayName = "Form";

const Title = styled.h2`
  margin-bottom: 30px;
  text-align: center;
`;
Title.displayName = "Title";

const RegisterContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: 40px;
  justify-content: space-between;
`;
RegisterContainer.displayName = "RegisterContainer";

const Text = styled.span`
  font-size: 13px;
`;
Text.displayName = "Text";

const RegisterLink = styled(Link)`
  font-size: 13px;
`;
RegisterLink.displayName = "RegisterLink";

const Login = props => {
  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    hasError
  } = props;

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Title>Login</Title>
        <Input
          id="username"
          type="email"
          name="username"
          label="Usuário"
          placeholder="Usuário"
          value={values.username}
          error={errors.username}
          touched={touched.username}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Input
          id="password"
          type="password"
          name="password"
          label="Senha"
          placeholder="Senha"
          value={values.password}
          error={errors.password}
          touched={touched.password}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        <Button type="submit" primary>Login</Button>
        {hasError && <ErrorMessage>Usuário ou senha inválido</ErrorMessage>}

        <RegisterContainer>
          <Text>Ainda não possui cadastro?</Text>
          <RegisterLink to={Routes.USERS_REGISTER}>Inscrever-se</RegisterLink>
        </RegisterContainer>
      </Form>
    </Container>
  );
};

export default Login;
