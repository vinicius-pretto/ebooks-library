import React from "react";
import { Formik } from "formik";

import BooksService from "../../services/BooksService";
import history from "../../router/history";
import Routes from "../../router/Routes";
import BooksForm from "../../components/BooksForm";
import Container from "../../components/Container";
import Title from "../../components/Title";
import validationSchema from "../../validations/bookSchema";

const initialValues = {
  name: "",
  author: "",
  language: "",
  publisher: "",
  year: "",
  pagesNumber: "",
  description: ""
};

class BooksRegister extends React.Component {
  constructor() {
    super();
    this.booksService = new BooksService();
    this.state = {
      uploadedFile: {}
    };
  }

  onChangeFile = async e => {
    const file = e.target.files[0];
    const uploadedFile = await this.booksService.uploadFile(file);
    this.setState({ uploadedFile });
  };

  onSubmit = async values => {
    const book = {
      name: values.name,
      description: values.description,
      pictureUrl: this.state.uploadedFile.src,
      details: {
        author: values.author,
        language: values.language,
        publisher: values.publisher,
        year: values.year,
        pagesNumber: values.pagesNumber
      }
    };

    try {
      await this.booksService.createBook(book);
      history.push(Routes.HOME);
    } catch (error) {
      console.error("Error on create a book", error);
    }
  };

  render() {
    return (
      <Container>
        <Title>Cadastro de livros</Title>
        <Formik
          initialValues={initialValues}
          onSubmit={this.onSubmit}
          validationSchema={validationSchema}
        >
          {props => {
            const {
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit
            } = props;

            return (
              <BooksForm
                onChangeFile={this.onChangeFile}
                uploadedFile={this.state.uploadedFile}
                values={values}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleBlur={handleBlur}
                handleSubmit={handleSubmit}
              />
            );
          }}
        </Formik>
      </Container>
    );
  }
}

export default BooksRegister;
