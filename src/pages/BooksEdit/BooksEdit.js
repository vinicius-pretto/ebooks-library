import React from "react";

import BooksForm from "../../components/BooksForm";

class BooksEdit extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const { name, description, details } = this.props.bookToEdit;
      const values = {
        name: name,
        author: details.author,
        language: details.language,
        publisher: details.publisher,
        year: details.year,
        pagesNumber: details.year,
        description: description
      }
      this.props.setValues(values);
    }, 800)
  }
  
  render() {
    const {
      values,
      errors,
      touched,
      handleChange,
      handleBlur,
      handleSubmit,
      onSubmit,
      onChangeFile,
      uploadedFile
    } = this.props;

    return (
      <BooksForm
        onSubmit={onSubmit}
        onChangeFile={onChangeFile}
        uploadedFile={uploadedFile}
        values={values}
        errors={errors}
        touched={touched}
        handleChange={handleChange}
        handleBlur={handleBlur}
        handleSubmit={handleSubmit}
      />
    );
  }
}

export default BooksEdit;
