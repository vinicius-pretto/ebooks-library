import React from "react";
import { Formik } from "formik";

import Container from "../../components/Container";
import Title from "../../components/Title";
import BooksEdit from './BooksEdit';
import bookSchema from "../../validations/bookSchema";
import BooksService from "../../services/BooksService";
import history from "../../router/history";
import Routes from "../../router/Routes";

class BooksEditContainer extends React.Component {
  constructor() {
    super();
    this.initialValues = {
      name: "",
      author: "",
      language: "",
      publisher: "",
      year: "",
      pagesNumber: "",
      description: ""
    }

    this.state = {
      bookId: '',
      bookToEdit: {}, 
      uploadedFile: {
        src: '',
        name: ''
      }
    };
    this.booksService = new BooksService();
  }

  async componentDidMount() {
    const bookId = this.props.match.params.id;

    if (bookId) {
      const bookToEdit = await this.booksService.getBookById(bookId);
      const uploadedFile = {
        src: bookToEdit.pictureUrl,
        name: bookToEdit.name
      }
      this.setState({ bookToEdit, uploadedFile, bookId });
    }
  }

  onChangeFile = async e => {
    const file = e.target.files[0];
    const uploadedFile = await this.booksService.uploadFile(file);
    this.setState({ uploadedFile });
  };

  onSubmit = async values => {
    const bookToUpdate = {
      name: values.name,
      description: values.description,
      pictureUrl: this.state.uploadedFile.src,
      details: {
        author: values.author,
        language: values.language,
        publisher: values.publisher,
        year: values.year,
        pagesNumber: values.pagesNumber
      }
    };
    await this.booksService.updateBook(this.state.bookId, bookToUpdate);
    history.push(Routes.BOOKS);
  }

  render() {
    return (
      <Container>
        <Title>Editar de livro</Title>

        <Formik
          initialValues={this.initialValues}
          onSubmit={this.onSubmit}
          validationSchema={bookSchema}
        >
          {props => {
            const {
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              setFieldValue,
              setValues
            } = props;

            return (
              <BooksEdit
                bookToEdit={this.state.bookToEdit}
                values={values}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleBlur={handleBlur}
                handleSubmit={handleSubmit}
                onChangeFile={this.onChangeFile}
                uploadedFile={this.state.uploadedFile}
                setFieldValue={setFieldValue}
                setValues={setValues}
              />
            )
          }}
        </Formik>
      </Container>
    );
  }
}

export default BooksEditContainer;
