import React from "react";

import Books from "./Books";
import BooksService from "../../services/BooksService";

class BooksContainer extends React.Component {
  constructor() {
    super();
    this.booksService = new BooksService();
    this.state = {
      bookToRemove: '',
      isModalOpen: false,
      books: []
    };
  }

  async componentDidMount() {
    await this.getBooks();  
  }

  getBooks = async () => {
    const books = await this.booksService.getBooks();
    this.setState({ books: books });
  }

  onRemoveButtonClick = (bookId) => {
    this.setState({ isModalOpen: true, bookToRemove: bookId });
  };

  onRemoveBook = async () => {
    await this.booksService.removeBook(this.state.bookToRemove);
    await this.getBooks();
    this.onCloseModal();
  }

  onCloseModal = () => {
    this.setState({ isModalOpen: false, bookToRemove: '' });
  };

  render() {
    const { isModalOpen, books } = this.state;

    return (
      <Books
        books={books}
        isModalOpen={isModalOpen}
        onRemoveButtonClick={this.onRemoveButtonClick}
        onRemoveBook={this.onRemoveBook}
        onCloseModal={this.onCloseModal}
      />
    );
  }
}

export default BooksContainer;
