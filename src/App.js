import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { connect } from 'react-redux';

import PrivateRoute from "./router/PrivateRoute";
import GlobalStyle from "./components/GlobalStyle";
import theme from "./styles/theme";
import Header from "./components/Header";
import Home from "./pages/Home";
import BookDetails from "./pages/BookDetails";
import MyBooks from "./pages/MyBooks";
import BooksRegister from "./pages/BooksRegister";
import Books from "./pages/Books";
import BooksEdit from "./pages/BooksEdit";
import Login from "./pages/Login";
import UsersRegister from "./pages/UsersRegister";
import NotFound from "./pages/NotFound";
import Routes from "./router/Routes";
import history from "./router/history";
import loginActions from "./redux/login/loginActions";
import AuthService from "./services/AuthService";

class App extends React.Component {
  constructor() {
    super();
    this.authService = new AuthService();
  }

  componentDidMount() {
    const currentUser = this.authService.getCurrentUser();
    const isAuthenticated = this.authService.isAuthenticated();
    this.props.setSession(currentUser, isAuthenticated)
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <React.Fragment>
          <GlobalStyle />
          <Router history={history}>
            <Header />
            <Switch>
              <PrivateRoute path={Routes.HOME} exact component={Home} />
              <PrivateRoute path={Routes.BOOK_DETAILS} component={BookDetails} />
              <PrivateRoute path={Routes.MY_BOOKS} component={MyBooks} />
              <PrivateRoute
                path={Routes.BOOKS_REGISTER}
                component={BooksRegister}
              />
              <PrivateRoute path={Routes.BOOKS} exact component={Books} />
              <PrivateRoute path={Routes.BOOKS_EDIT} component={BooksEdit} />
              <Route
                path={Routes.USERS_REGISTER}
                component={UsersRegister}
              />
              <Route path={Routes.LOGIN} component={Login} />
              <Route component={NotFound} />
            </Switch>
          </Router>
        </React.Fragment>
      </ThemeProvider>
    );
  }
}

const stateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    setSession: (currentUser, isAuthenticated) => dispatch(loginActions.setSession(currentUser, isAuthenticated))
  }
}

export default connect(stateToProps, mapDispatchToProps)(App);
