import types from './types';

const initialState = {
  books: [],
  isModalOpen: false,
  deliveryDate: ''
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.SET_BOOKS:
      return {
        ...state,
        books: action.payload.books
      };
    case types.OPEN_MODAL:
      return {
        ...state,
        isModalOpen: true
      };
    case types.CLOSE_MODAL:
      return {
        ...state,
        isModalOpen: false
      };
    case types.SET_DELIVERY_DATE:
      return {
        ...state,
        deliveryDate: action.payload.deliveryDate
      }
    default:
      return state;
  }
}
