import React from "react";
import styled from "styled-components";

import Label from "../Label";
import ErrorMessage from '../ErrorMessage';
import FormGroup from '../FormGroup';

const TextAreaContainer = styled.textarea`
  height: 150px;
  padding: 10px;
  border: 1px solid;
  border-radius: 4px;
  border-color: ${props => props.hasError ? props.theme.error : props.theme.gray};
  outline-color: ${props => props.theme.primary};
  font-family: Roboto, Helvetica, "Helvetica Neue", Arial, sans-serif;

  &::placeholder {
    font-family: Roboto, Helvetica, "Helvetica Neue", Arial, sans-serif;
  }
`;
TextAreaContainer.displayName = "TextAreaContainer";

const TextArea = props => {
  const {
    label,
    id,
    name,
    value,
    placeholder,
    error,
    touched,
    onChange,
    onBlur
  } = props;

  const hasError = error && touched;

  return (
    <FormGroup>
      <Label htmlFor={id} hasError={hasError}>{label}</Label>
      <TextAreaContainer
        id={id}
        name={name}
        value={value}
        placeholder={placeholder}
        hasError={hasError}
        error={error}
        touched={touched}
        onChange={onChange}
        onBlur={onBlur}
      />
      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </FormGroup>
  );
};

export default TextArea;
