import React from "react";
import styled from "styled-components";

// import Search from "../Search";
import Device from "../../../styles/Device";
import history from "../../../router/history";

const Container = styled.nav`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;
Container.displayName = "Container";

const NavMenu = styled.nav`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: ${props => (props.isMenuOpen ? "0" : "-100%")};
  left: 0;
  background: ${props => props.theme.primary};
  z-index: 9;
  transition: top 0.2s ease-out 0s;
`;
NavMenu.displayName = "NavMenu";

const MenuHeader = styled.div`
  display: flex;
  background: ${props => props.theme.primary};
  height: 62px;
`;
MenuHeader.displayName = "MenuHeader";

const MenuHeaderContainer = styled.div`
  display: flex;
  flex: 1;
  margin-left: 15px;
  margin-right: 15px;
  align-items: center;
  justify-content: flex-end;
`;
MenuHeaderContainer.displayName = "MenuHeaderContainer";

const CloseButton = styled.button`
  width: 20px;
  height: 20px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  background-image: url("/icons/times.svg");
`;
CloseButton.displayName = "CloseButton";

const Title = styled.h1`
  font-size: 14px;
  color: #fff;
`;
Title.displayName = "Title";

const Menu = styled.ul`
  margin-left: 38px;
`;
Menu.displayName = "Menu";

const MenuItem = styled.li`
  color: #fff;
  padding: 20px 0;
`;
MenuItem.displayName = "MenuItem";

const MenuItemLink = styled.a`
  color: #fff;
  font-size: 20px;
`;
MenuItemLink.displayName = "MenuItemLink";

const MenuButton = styled.button`
  width: 20px;
  height: 20px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  margin-left: 15px;
  background-image: url("/icons/burger-menu.svg");

  @media ${Device.MOBILE} {
    margin-left: 8px;
  }
`;
MenuButton.displayName = "MenuButton";

class MobileNavbarMenu extends React.Component {
  state = {
    isOpen: false
  };

  onMenuClick = () => {
    this.setState({ isMenuOpen: true });
  };

  onCloseMenu = () => {
    this.setState({ isMenuOpen: false });
  };

  onLinkClick = path => {
    this.setState({ isMenuOpen: false });
    history.push(path);
  };

  render() {
    return (
      <React.Fragment>
        <NavMenu isMenuOpen={this.state.isMenuOpen}>
          <MenuHeader>
            <MenuHeaderContainer>
              <CloseButton
                type="button"
                aria-label="Fechar Menu"
                title="Fechar"
                onClick={this.onCloseMenu}
              />
            </MenuHeaderContainer>
          </MenuHeader>
          <Menu>
            {this.props.menuOptions.map((option, key) => (
              <MenuItem key={key}>
                <MenuItemLink
                  onClick={() => this.onLinkClick(option.href)}
                  aria-label={option.title}
                >
                  {option.title}
                </MenuItemLink>
              </MenuItem>
            ))}
          </Menu>
        </NavMenu>

        <Container>
          {/* <Search /> */}
          <MenuButton
            type="button"
            aria-label="Menu"
            title="Menu"
            onClick={this.onMenuClick}
          />
        </Container>
      </React.Fragment>
    );
  }
}

export default MobileNavbarMenu;
