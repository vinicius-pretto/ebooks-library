import React from "react";
import styled from "styled-components";
import Media from "react-media";
import { connect } from "react-redux";
import _get from "lodash/get";

import NavbarMenu from "./NavbarMenu";
import MobileNavbarMenu from "./MobileNavbarMenu";
import Device from "../../styles/Device";
import AuthService from "../../services/AuthService";
import Routes from "../../router/Routes";
import history from "../../router/history";
import UserRole from "../../userRole";
import mobileMenuOptions from "./mobileMenuOptions";
import desktopMenuOptions from "./desktopMenuOptions";

const Title = styled.h1`
  font-size: 16px;
  font-weight: bold;
  display: inline-block;
  vertical-align: top;
  margin-right: 67px;

  @media ${Device.LAPTOP_EXTRA_SMALL} {
    margin-right: auto;
  }
  @media ${Device.TABLET} {
    font-size: 14px;
  }
`;
Title.displayName = "Title";

const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  background: ${props => props.theme.primary};
  color: #fff;
  height: 62px;

  @media ${Device.MOBILE} {
    justify-content: space-between;
  }
`;
HeaderContainer.displayName = "HeaderContainer";

const Container = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
  margin-left: 65px;
  margin-right: 65px;

  @media ${Device.LAPTOP_SMALL} {
    margin-left: 20px;
    margin-right: 20px;
  }
  @media ${Device.TABLET_LARGE} {
    margin-left: 15px;
    margin-right: 15px;
  }
`;
Container.displayName = "Container";

class Header extends React.Component {
  constructor() {
    super();
    this.authService = new AuthService();
  }

  onSignOut = () => {
    history.push(Routes.LOGIN);
  };

  render() {
    const { currentUser, isAuthenticated } = this.props;
    const userRole = _get(currentUser, "role", UserRole.USER);

    return (
      <HeaderContainer>
        <Container>
          <Title>Ebooks Library</Title>
          {isAuthenticated && (
            <Media query={Device.LAPTOP_EXTRA_SMALL}>
              {isMobile =>
                isMobile ? (
                  <MobileNavbarMenu menuOptions={mobileMenuOptions[userRole]} />
                ) : (
                  <NavbarMenu
                    onSignOut={this.onSignOut}
                    menuOptions={desktopMenuOptions[userRole]}
                  />
                )
              }
            </Media>
          )}
        </Container>
      </HeaderContainer>
    );
  }
}

const stateToProps = state => {
  return {
    isAuthenticated: state.login.isAuthenticated,
    currentUser: state.login.currentUser
  };
};

export default connect(stateToProps)(Header);
