import UserRole from '../../userRole';
import menuOptions from './menuOptions';

const desktopMenuOptions = {
  [UserRole.USER]: [
    menuOptions.HOME,
    menuOptions.MY_BOOKS
  ],
  [UserRole.ADMIN]: [
    menuOptions.HOME,
    menuOptions.MY_BOOKS,
    menuOptions.BOOKS_REGISTER,
    menuOptions.BOOKS
  ]
}

export default desktopMenuOptions;
