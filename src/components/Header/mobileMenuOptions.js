import UserRole from '../../userRole';
import menuOptions from './menuOptions';

const mobileMenuOptions = {
  [UserRole.USER]: [
    menuOptions.HOME,
    menuOptions.MY_BOOKS,
    menuOptions.LOGIN
  ],
  [UserRole.ADMIN]: [
    menuOptions.HOME,
    menuOptions.MY_BOOKS,
    menuOptions.BOOKS_REGISTER,
    menuOptions.BOOKS,
    menuOptions.LOGIN
  ]
}

export default mobileMenuOptions;
