import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

// import Search from "../Search";

const NavMenu = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex: 1;
`;
NavMenu.displayName = "NavMenu";

const Menu = styled.ul`
  display: flex;
`;
Menu.displayName = "Menu";

const MenuItem = styled.li`
  margin: 0 10px;
`;
MenuItem.displayName = "MenuItem";

const MenuItemLink = styled(NavLink)`
  color: #fff;
  font-size: 14px;
`;
MenuItemLink.displayName = "MenuItemLink";

const ButtonsContainer = styled.div`
  display: flex;
  align-items: center;
`;
ButtonsContainer.displayName = "ButtonsContainer";

const SignOut = styled.button`
  width: 20px;
  height: 20px;
  border: none;
  cursor: pointer;
  background: transparent;
  background-repeat: no-repeat;
  background-image: url("/icons/sign-out.svg");
  margin-left: 15px;
  margin-top: 5px;
`;
SignOut.displayName = "SignOut";

const NavbarMenu = ({ onSignOut, menuOptions }) => {
  return (
    <NavMenu>
      <Menu>
        {menuOptions.map((option, key) =>
          <MenuItem key={key}>
            <MenuItemLink to={option.href} aria-label={option.title}>
              {option.title}
            </MenuItemLink>
          </MenuItem>    
        )}
      </Menu>

      <ButtonsContainer>
        {/* <Search /> */}
        <SignOut type="button" aria-label="Sair" title="Sair" onClick={onSignOut} />
      </ButtonsContainer>
    </NavMenu>
  );
};

export default NavbarMenu;
