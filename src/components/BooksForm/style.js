import Device from "../../styles/Device";
import styled from "styled-components";

export const FormContainer = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-end;

  @media ${Device.TABLET} {
    flex-direction: column;
  }
`;
FormContainer.displayName = "FormContainer";

export const Form = styled.form`
  width: 600px;

  @media ${Device.TABLET} {
    width: 100%;
  }
`;
Form.displayName = "Form";

export const SubmitButton = styled.button`
  background: ${props => props.theme.primary};
  color: #fff;
  box-sizing: border-box;
  border: 1px solid ${props => props.theme.primary};
  font-size: 13px;
  border-radius: 4px;
  padding: 13px 20px;
  text-transform: uppercase;
  width: 170px;
  cursor: pointer;

  @media ${Device.TABLET} {
    width: 100%;
  }
`;
SubmitButton.displayName = "SubmitButton";

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;
ButtonContainer.displayName = "ButtonContainer";

export const UploadContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-left: 15px;

  @media ${Device.TABLET} {
    margin: 20px 0 30px 0;
    align-items: center;
  }
`;
UploadContainer.displayName = "UploadContainer";

export const UploadButton = styled.input``;
UploadButton.displayName = "UploadButton";

export const UploadedImage = styled.img`
  margin-bottom: 10px;
  max-width: 166px;
`
UploadedImage.displayName = 'UploadedImage';
