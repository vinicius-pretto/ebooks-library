import styled from "styled-components";
import Device from "../../styles/Device";

const Title = styled.h1`
  font-size: 28px;
  color: ${props => props.theme.text};
  font-weight: normal;
  margin-bottom: 18px;

  @media ${Device.TABLET} {
    font-size: 24px;
    margin-bottom: 10px;
  }
`;
Title.displayName = "Title";

export default Title;
