const app = require("./server/src/app");
const config = require("./config");

async function bootstrap() {
  await app.listen(config.port);
  console.log(`Server is listening at port ${config.port}`);
}

bootstrap();
