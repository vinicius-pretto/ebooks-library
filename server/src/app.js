const express = require("express");
const path = require("path");
const config = require("../../config");
const app = express();

app.use(express.static(path.resolve(config.staticFolder), config.staticConfig));

module.exports = app;
