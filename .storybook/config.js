import React from "react";
import { configure, addDecorator } from "@storybook/react";
import theme from "../src/styles/theme";
import { ThemeProvider } from "styled-components";
import GlobalStyle from "../src/components/GlobalStyle";
import { addParameters } from "@storybook/react";
import { themes } from "@storybook/theming";
import { BrowserRouter } from "react-router-dom";

// Option defaults.
addParameters({
  options: {
    theme: themes.dark
  }
});

const ThemeDecorator = story => (
  <ThemeProvider theme={theme}>
    <React.Fragment>
      <GlobalStyle />
      <BrowserRouter>{story()}</BrowserRouter>
    </React.Fragment>
  </ThemeProvider>
);

// automatically import all files ending in *.stories.js
const req = require.context("../stories", true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
addDecorator(ThemeDecorator);
