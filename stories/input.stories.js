import React from "react";
import { storiesOf } from "@storybook/react";
import Input from "../src/components/Input";

const stories = storiesOf("Input", module);

stories.add("Default", () => (
  <Input id="name" type="text" label="Name" placeholder="Name" />
));
